# Problem

- Inconsistent behaviour of docker registry mirrors
- Insecure registries
- Untrusted TLS
- Build cache persistence
- Limited Buildkit support in Docker
- Docker image references should be 
  - fully qualified, except (optional) `docker.io` registry, and its `library/*` 
    namespace in particular 
  - portable (globally unique name, not a network address, 
    e.g. `docker.example.com/group/subgroup/image:tag`,
    not `registry:5000/group/subgroup/image:tag`) 

# Solution

Registry mirrors as a configuration property of docker services are not used at all.

All supported registries, resp. their hostnames are aliased to `https://localhost` via `/etc/hosts` so that all explicit requests to external registries can be intercepted and re-routed to globally configured mirror.

Local `haproxy` is configured to intercept all requests and forward them to configured mirror (or the actual master registry).

Well known HTTPS port `443` on `https://localhost` is secured using dynamically generated certificate supporting all configured registries (via X509 Subject Alternative Name attribute), and issued using trusted local CA. 




# Configuration

```yaml
# following registries will be redirected to configured mirror
registries:
  - https://registry-v1.docker.io
  - https://mcr.microsoft.com
  - https://docker.elastic.co 

# mirror is expected to be a caching proxy supporting all above mentioned registries
mirror:
  name: docker.example.com
  url: http://docker.example.local:8080

# publish images in snapshots/releases registry; they should be accessible via mirror
snapshots:
  name: docker-snapshots.example.com
  url: http://docker.example.com:8081
releases:
  name: docker-releases.example.com
  url: http://docker.example.local:8082

# credentials for registries that need them; used to generate ~/.docker/config.json {auths:{}} section
auth:
  - id: https://index.docker.io/v1/
    username: 
    password:
  - id: docker-snapshots.example.com
    username:
    password:
  - id: docker-releases.example.com
    username:
    password:
  - id: docker.example.com
    username:
    password:       
```

YAML configuration data can be passed to runtime
 - via `XCONFIG` environment property
 - via `/etc/xconfig.yaml` file
 
# Using Buildkit

```shell script
buildctl \
  --frontend gateway.v0 \
  --opt source=docker.io/docker/dockerfile:latest \ 
  --local context=. \
  --local dockerfile=. \
  --opt build-arg:key=value \
  --output type=image,name=docker-snapshots/group/repo:tag,push=true \
  --export-cache type=registry,ref=cache.local/group/repo:cache \
  --import-cache type=registry,ref=cache.local/group/repo:cache \
  "$@"   
```