
FROM alpine as download-gitlab-runner
ARG GITLAB_RUNNER_VERSION=12.5.0
RUN echo "Downloading gitlab-runner: ${GITLAB_RUNNER_VERSION}" &&\
    fname="/usr/local/bin/gitlab-runner" &&\
    url="https://s3.amazonaws.com/gitlab-runner-downloads/v${GITLAB_RUNNER_VERSION}/binaries/gitlab-runner-linux-386" &&\
    echo "$url" &&\
    wget -O $fname $url &&\
    chmod +x $fname

FROM alpine as download-buildkit
WORKDIR /tmp/
ARG BUILDKIT_VERSION=0.6.3
RUN echo "Downloading buildkit ${BUILDKIT_VERSION}" &&\
    url="https://github.com/moby/buildkit/releases/download/v${BUILDKIT_VERSION}/buildkit-v${BUILDKIT_VERSION}.linux-amd64.tar.gz" &&\
    echo "${url}" &&\
    wget -O buildkit.tgz ${url}
RUN tar xzvf buildkit.tgz -C /usr/local/bin/ --strip-components=1

FROM alpine as download-registry
ARG REGISTRY_VERSION=2.7.1
RUN echo "Downloading docker registry ${REGISTRY_VERSION}" &&\
    url="https://github.com/docker/distribution-library-image/raw/registry-${REGISTRY_VERSION}/amd64/registry" &&\
    echo "$url" &&\
    wget -O /usr/local/bin/registry ${url} &&\
    chmod a+x /usr/local/bin/registry

#todo hardcoded specific versions combo, working on alpine. test this with latest releases & upgrade if possible
FROM mcr.microsoft.com/dotnet/core/sdk:3.0-alpine as download-gitversion
WORKDIR /work
RUN apk add git && git clone https://github.com/GitTools/GitVersion.git && cd GitVersion && git reset --hard 52a5c55
RUN cd GitVersion/src/GitVersionExe && dotnet publish \
        -f netcoreapp3.0 -r linux-musl-x64 \
        /p:PackageVersion_LibGit2Sharp=0.27.0-preview-0034 /p:PackageVersion_LibGit2Sharp_NativeBinaries=2.0.298
RUN git clone --recursive https://github.com/libgit2/libgit2sharp.nativebinaries &&\
    cd libgit2sharp.nativebinaries &&\
    git reset --hard 96eb4c0 &&\
    git submodule init &&\
    git submodule update
RUN apk add bash build-base cmake openssl-dev rsync
RUN mkdir /nativebinaries && rsync -r libgit2sharp.nativebinaries/ /nativebinaries/
RUN apk add ncurses
RUN cd /nativebinaries && bash -c "RID=alpine.3.9-x64 TERM=xterm ./build.libgit2.sh"
RUN find / -name "libgit2*.so" | xargs ls -ltr
RUN rsync /nativebinaries/libgit2/build/ /work/GitVersion/src/GitVersionExe/bin/Debug/netcoreapp3.0/linux-musl-x64/publish/
RUN echo 1
RUN rsync -rv /work/GitVersion/src/GitVersionExe/bin/Debug/netcoreapp3.0/linux-musl-x64/publish/ /opt/gitversion/ &&\
    cp -v /nativebinaries/libgit2/build/*.so* /opt/gitversion/ &&\
    ln -s /opt/gitversion/GitVersion /usr/local/bin/gitversion
RUN apk add libstdc++ libstdc++6 libgit2
RUN find /nativebinaries/ -name "*so"
RUN cd /work/GitVersion && DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=true LD_LIBRARY_PATH=/opt/gitversion:${LD_LIBRARY_PATH} LD_DEBUG=true gitversion

FROM alpine as download-cni
RUN apk add curl
WORKDIR /work
ARG CNI_VERSION=0.8.3
RUN curl -LO "https://github.com/containernetworking/plugins/releases/download/v${CNI_VERSION}/cni-plugins-linux-amd64-v${CNI_VERSION}.tgz"
RUN mkdir -p /opt/cni/bin && tar xzvf cni*tgz -C /opt/cni/bin/

FROM alpine as download-buildx
RUN apk add curl
ARG BUILDX_VERSION=0.3.1
WORKDIR /work
RUN curl -LO "https://github.com/docker/buildx/releases/download/v${BUILDX_VERSION}/buildx-v${BUILDX_VERSION}.linux-amd64"
RUN mv buildx* /usr/local/bin/buildx && chmod a+x /usr/local/bin/buildx

FROM alpine as download-daemonize
WORKDIR /work
RUN apk add build-base git
ARG DAEMONIZE_VERSION="release-1.7.8"
RUN git clone https://github.com/bmc/daemonize.git --branch $DAEMONIZE_VERSION --single-branch
RUN cd daemonize && ./configure && make && make install

# layer0 / base
FROM docker:dind as base
RUN --mount=type=cache,target=/var/cache/apk \
    --mount=type=cache,target=/root/.cache/pip \
    ln -s /var/cache/apk /etc/apk/cache &&\
    apk update -U &&\
    apk upgrade &&\
    apk add openssl &&\
    apk add bash bash-completion curl vim git htop &&\
    apk add jq py-pip && pip install yq &&\
    apk add haproxy containerd sudo &&\
    apk add libstdc++ libstdc++6 libgit2 &&\
    mkdir /etc/gitlab-runner && touch /etc/gitlab-runner/config.toml &&\
    adduser -D ci

# layer1: core utilities: consolidate / prepare for copy
FROM alpine as download1
COPY --from=download-gitversion /opt/gitversion/ /opt/gitversion/
COPY --from=download-gitversion /usr/local/bin/ /usr/local/bin/
COPY --from=download-daemonize /usr/local/sbin/daemonize /usr/local/sbin/daemonize
COPY --from=download-cni /opt/cni/bin/ /opt/cni/bin/
COPY --from=download-registry /usr/local/bin/ /usr/local/bin/
COPY --from=download-buildx /usr/local/bin/ /usr/local/bin/

# layer2: buildkit + runner: consolidate / prepare for copy
FROM alpine as download2
COPY --from=download-buildkit /usr/local/bin/ /usr/local/bin/
COPY --from=download-gitlab-runner /usr/local/bin/ /usr/local/bin/

# and put it all together
FROM base as main
ENV DOCKER_BUILDKIT=1 \
    DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=true \
    LD_LIBRARY_PATH=/opt/gitversion:${LD_LIBRARY_PATH}
CMD [ "/usr/local/bin/entrypoint.sh", "main" ]
# layer1: core utils/addons
RUN --mount=from=download1,source=/,target=/download \
    echo "gitversion, registry, builx, cni" &&\
    tar c -C /download \
        usr/local/sbin/ \
        usr/local/bin/ \
        opt/gitversion \
        opt/cni \
    | tar xv -C /
# layer2: buildkit+runner
RUN --mount=from=download2,source=/,target=/download \
    echo "buildkit, gitlab-runner" &&\
    tar c -C /download \
        usr/local/bin/ \
        etc/buildkit \
    | tar xv -C / &&\
    mkdir -p ~/.docker/cli-plugins && ln -s /usr/local/bin/buildx ~/.docker/cli-plugins/docker-buildx
# layer3: scripts+configs
RUN --mount=id=context,source=/,target=/context \
    mkdir -p /etc/buildkit/ ~/.docker/cli-plugins/ &&\
    echo "configs, scripts" &&\
    cd /context &&\
    cp -v cni.json /etc/buildkit/cni.json &&\
    cp -v daily.sh /etc/periodic/daily/cleanup &&\
    cp -v entrypoint.sh pki.sh /usr/local/bin/

