#!/bin/bash

ENABLE_DOCKERD=${ENABLE_DOCKERD:-true}
ENABLE_BUILDKITD=${ENABLE_BUILDKITD:-true}

${ENABLE_BUILDKITD} && buildctl prune --all

${ENABLE_DOCKERD} && docker ps -qa | xargs -r docker rm -f
${ENABLE_DOCKERD} && docker system prune -af --volumes

rm -rf /var/lib/registry/*
rm -rf /builds/*
