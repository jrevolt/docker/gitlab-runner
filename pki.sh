#!/bin/bash
set -u

ORG="/O=JRevolt/OU=Research & Development/L=Bratislava/C=SK"
DOMAIN="/DC=cloud/DC=local"
CA_ROOT="/CN=Cloud CA Root"
CA_ISSUING="/CN=Cloud CA Issuing"
TLS="/CN=*.cloud.local"

KEYSIZE=2048

key() {
	local name="$1"
	[ -f ${name}.key ] || openssl genrsa -out ${name}.key $KEYSIZE
}

csr() {
	local name="$1"
	local subj="$2"
	[ -f ${name}.csr ] || openssl req -sha256 -new -key ${name}.key -subj "${subj}" -out ${name}.csr
}

crt() {
	local name="$1"
	local signer="$2"
	local validity="$3"
	shift 3
	if [[ "$name" = "$signer" ]]; then
		openssl x509 -req -sha256 -in ${name}.csr \
			-signkey "${signer}.key" -CAcreateserial -days $validity \
			-out ${name}.crt "$@"
	else
		openssl x509 -req -sha256 -in ${name}.csr \
			-CA "${signer}.crt" -CAkey "${signer}.key" -CAcreateserial -days $validity \
			-out ${name}.crt "$@"
	fi
	cat ca*crt > cacerts.pem
}

all() {
   	local name="$1"
   	local signer="$2"
   	local subj="$3"
   	local validity="$4"
   	shift 4
   	key "$name"
   	csr "$name" "$subj"
   	crt "$name" "$signer" "$validity" "$@"
   	ls -la ${name}.{key,crt}
}

ca() {
	local name="ca"
	local subj="${CA_ROOT}${DOMAIN}${ORG}"
	local conf="$(mktemp)"; trap "rm -f $conf" EXIT
	cat > $conf \
<< EOF
[ ext ]
keyUsage                = critical,keyCertSign,cRLSign
basicConstraints        = critical,CA:true
subjectKeyIdentifier    = hash
EOF
	all "$name" "$name" "$subj" 9999 -extensions ext -extfile ${conf}
}

ca1() {
	local name="ca1"
	local subj="${CA_ISSUING}${DOMAIN}${ORG}"
	local conf="$(mktemp)"; trap "rm -f $conf" EXIT
	cat > $conf \
<< EOF
[ ext ]
keyUsage                = critical,keyCertSign,cRLSign
basicConstraints        = critical,CA:true,pathlen:0
subjectKeyIdentifier    = hash
EOF
	all "$name" "ca" "$subj" 3650 -extensions ext -extfile $conf
}


tls() {
  local domain="$1"
  local conf="/tmp/${domain}.conf"
  local idx=0
  ca
  ca1
  cat > ${conf} \
<<EOF
[ ext ]
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth,clientAuth
subjectAltName = @alt_names
[alt_names]
DNS.$((++idx)) = localhost
DNS.$((++idx)) = cache.local
$(
for i in $(get_config | yq -r '.mirror.name,.snapshots.name,.releases.name,.registries[]'); do
  parse_url ${i}
  echo "DNS.$((++idx)) = ${host}"
done
)
EOF
  local subj="/CN=${domain}${DOMAIN}${ORG}"
  all "$domain" "ca1" "$subj" 999 -extensions ext -extfile $conf
}

