#!/bin/bash
set -u

source $(dirname $0)/pki.sh

ENABLE_CROND=${ENABLE_CROND:-true}
ENABLE_REGISTRY=${ENABLE_REGISTRY:-true}
ENABLE_CONTAINERD=${ENABLE_CONTAINERD:-true}
ENABLE_DOCKERD=${ENABLE_DOCKERD:-true}
ENABLE_BUILDKITD=${ENABLE_BUILDKITD:-true}

get_config() {
  echo "${XCONFIG:-$(cat /etc/xconfig.yaml)}"
}

pki() {
  (
  cd /tmp
  tls local
  cp ca.crt ca1.crt /usr/local/share/ca-certificates/
  update-ca-certificates
  cat local.key local.crt ca1.crt ca.crt > /etc/haproxy/tls.pem
  )
}

parse_url() {
  if [ "$1" = "null" ]; then unset host port proto; return; fi
  proto=$(echo $1 | sed -re 's/:.*//; s/(^https?)?.*/\1/; s/^$/https/')
  host=$(echo $1 | sed -re 's/^https?:\/\///; s/:.*//')
  port=$(echo $1 | sed -re 's/^https?:\/\///; s/^[^:]*:?//')
  if [[ "$port" == "" ]]; then port=$(echo "$proto" | sed 's/https/443/; s/http/80/'); fi
}

parse_mirror_url() {
  parse_url $(get_config | yq -r .mirror.url)
  if [ "${host:-}" = "" ]; then return; fi
  mirror_proto=${proto}
  mirror_host=${host}
  mirror_port=${port}
}

parse_snapshots_url() {
  parse_url $(get_config | yq -r .snapshots.url)
  snapshots_proto=${proto}
  snapshots_host=${host}
  snapshots_port=${port}
}

parse_releases_url() {
  parse_url $(get_config | yq -r .releases.url)
  releases_proto=${proto}
  releases_host=${host}
  releases_port=${port}
}

cfg_haproxy() {
  parse_mirror_url
  parse_snapshots_url
  parse_releases_url
  cat \
<<EOF
global
  log stdout format raw local0
  tune.ssl.default-dh-param 2048
  resolvers mydns
    nameserver dflt $(cat /etc/resolv.conf | grep nameserver | head -n1 | sed 's/.* //'):53
defaults
  log global
  mode http
  timeout connect 10s
  timeout client 30s
  timeout server 30s
  default-server init-addr none resolvers mydns
frontend mirror
  bind 0.0.0.0:80
  bind 0.0.0.0:443 ssl crt /etc/haproxy/tls.pem
  option httplog
  use_backend mirror    if { hdr(Host) -i $(get_config | yq -r .mirror.name) }
  use_backend snapshots if { hdr(Host) -i $(get_config | yq -r .snapshots.name) }
  use_backend releases  if { hdr(Host) -i $(get_config | yq -r .releases.name) }
  use_backend registry  if { hdr(Host) -i cache.local }
$(
for i in $(get_config | yq -r '.registries[]'); do
parse_url ${i}
cat <<EOF1
  use_backend ${host} if { hdr(Host) -i ${host} }
EOF1
done
)
$(
if [[ "${mirror_host:-}" != "" ]]; then
cat <<EOF1
backend mirror
  #http-request set-header Authorization "Basic \${MIRROR_BASIC_AUTH}"
  server  ${mirror_host} ${mirror_host}:${mirror_port} check $(echo ${mirror_proto} | sed 's/https/ssl verify none/; s/http//')
EOF1
fi
)
backend snapshots
  server ${snapshots_host} ${snapshots_host}:${snapshots_port} check $(echo ${snapshots_proto} | sed 's/https/ssl verify none/; s/http//')
backend releases
  server ${releases_host} ${releases_host}:${releases_port} check $(echo ${releases_proto} | sed 's/https/ssl verify none/; s/http//')
backend registry
  server registry 127.0.0.1:5000 check
$(
parse_mirror_url;
for i in $(get_config | yq -r '.registries[]'); do
parse_url ${i}
cat <<EOF2
backend ${host}
  server ${host} ${mirror_host:-$host}:${mirror_port:-$port} check $(echo ${mirror_proto:-$proto} | sed 's/https/ssl verify none/; s/http//')
EOF2
done
)
EOF
}

cfg_etc_hosts() {
local names=$(get_config | yq -r '.mirror.name,.snapshots.name,.releases.name,.registries[]')
cat <<EOF
$(cat /etc/hosts.bak)
127.0.0.1 localhost cache.local $(for i in ${names}; do parse_url ${i}; echo -n "$host "; done)
EOF
}

cfg_registry() {
cat <<EOF
version: 0.1
storage:
  filesystem:
    rootdirectory: /var/lib/registry
  delete:
    enabled: true
http:
  addr: :5000
  headers:
    X-Content-Type-Options: [nosniff]
EOF
}

gen_haproxy_config() {
  cfg_haproxy | tee /etc/haproxy/haproxy.cfg
}

gen_registry_config() {
  mkdir -p /etc/docker/registry
  mkdir -p /var/lib/registry
  cfg_registry > /etc/docker/registry/config.yml
}

gen_docker_client_config2() {
  mkdir -p $HOME/.docker
  get_config \
  | yq '.auth[] | { (.id): { auth: (.username + ":" + .password | @base64) } }' \
  | jq -s 'add | { auths: . }' \
  > ~/.docker/config.json
}

fix_etc_hosts() {
  [[ -f /etc/hosts.bak ]] || cat /etc/hosts > /etc/hosts.bak
  cfg_etc_hosts | tee /etc/hosts
}

register_gitlab_runner() {
  [ -f /etc/gitlab-runner/config.toml ] && return 0

  gitlab-runner register -n ${RUNNER_REGISTRATION_OPTS:-}
}

space() {
    du -sch /var/lib/buildkit/ /var/lib/docker/ /var/lib/registry/ /builds/
}

cleanup() {
  buildctl prune --all; docker ps -qa | xargs docker rm -f; docker system prune -af --volumes; rm -rf /var/lib/registry/* /builds/*
}

hardreset() {
    killall dockerd buildkitd containerd
    sleep 5s
    rm -rf /var/lib/buildkit/* /var/lib/docker/* /var/lib/registry/* /builds/*
}

fail() {
  echo "## [ERR] $@" && exit 1
}

fail_wlog() {
  local logfile="$1"; shift
  echo "## ${logfile}"
  tail -n10 ${logfile}
  fail "$@"
}

pcheck() {
  local svc="$1"
  pgrep ${svc} >/dev/null && echo "OK: ${svc}" || {
    log="/var/log/${svc}.log"
    [[ -f ${log} ]] && {
      echo "## ${log}"
      tail -n10 ${log}
    }
    echo "## [ERR] ${svc} not running"
    return 1
  }
}

healthcheck() {
  local ok=true
  pcheck haproxy || ok=false
  pcheck gitlab-runner || ok=false
  ${ENABLE_BUILDKITD} && { pcheck buildkitd || ok=false; }
  ${ENABLE_DOCKERD} && { pcheck dockerd || ok=false; }
  ${ENABLE_CONTAINERD} && { pcheck containerd || ok=false; }
  ${ENABLE_REGISTRY} && { pcheck registry || ok=false; }
  ${ENABLE_CROND} && { pcheck crond || ok=false; }
  ${ok} && return 0
}

daemonize() {
  local log="/var/log/${1}.log"
  local pathname="$(which ${1})"; shift
  $(which daemonize) -o ${log} -e ${log} ${pathname} "$@"
}

main() {

  pki
  fix_etc_hosts
  gen_haproxy_config
  gen_registry_config
  gen_docker_client_config2
  register_gitlab_runner

  trap "killall haproxy buildkitd dockerd containerd crond gitlab-runner registry tail bash" EXIT

  ${ENABLE_CROND} &&
  echo "Starting crond..." &&
  daemonize crond -f -d8

  ${ENABLE_REGISTRY} &&
  echo "Starting registry..." &&
  daemonize registry serve /etc/docker/registry/config.yml

  ${ENABLE_CONTAINERD} &&
  echo "Starting containerd..." &&
  daemonize containerd

  ${ENABLE_CONTAINERD} &&
  while pgrep containerd >/dev/null && [[ ! -S /run/containerd/containerd.sock ]]; do sleep 1s; done

  ${ENABLE_DOCKERD} &&
  echo "Starting dockerd..." &&
  daemonize dockerd -D

  ${ENABLE_BUILDKITD} &&
  echo "Starting buildkitd..." &&
  daemonize buildkitd --oci-worker=true --oci-worker-gc=false --oci-worker-net=cni --tlscacert /etc/ssl/certs/ca-certificates.crt --containerd-worker=false

  echo "Starting haproxy..." &&
  daemonize haproxy -f /etc/haproxy/haproxy.cfg -db

  echo "Starting gitlab-runner..." &&
  gitlab-runner run
}

"$@"
